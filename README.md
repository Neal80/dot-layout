# dot-layout

Extended [doT](http://olado.github.com/doT) support layout and subdirectory.

## Installation

    npm i dot-layout --save

## Features
    custom file extension: htm, html...
    layout template.
    subdirectory in template folder.
    clean complied folder on next complie.


## Usage:

```js
const cluster = require('cluster');
const path = require('path');
const dot = require('dot-layout');

const options = {
    path: path.join(__dirname, 'templates'),
    destination: path.join(__dirname, 'views'),
};

let dots = {};
if (cluster.isMaster) {

    dots = dot.process(options);

} else {

    dots = dot.load(options);

}


```

You can useing subdirectory in template folder, like:

    ...
    templates
        |-- layouts
        |   |-- home.ly.html  // for home page
        |   |-- other.ly.html  // for user account
        |   |-- partial.ly.html  // for paritals
        |
        |-- partials
        |   |-- render_on_server_partial.def.html
        |   |-- render_in_browser_partial.def.html
        |
        |-- home.dot.jst.html  // {{#def.partials.some_partial}}
    ...


```html
home.ly.html

<html>
    <head>...</head>
    <body>
        {{#body}}
    </body>
</html>
```

```html
home.dot.jst.html

{{#layout.layouts/home}}

<h2>Home page</h2>
<hr/>

{{#def.partials.some_partial}}
```


Render in browser, see [With express](https://gitlab.com/Neal80/dot-layout/tree/master/examples) 
```html
{{#layout.layouts/home}}

<script type="text/javascript" src="partial.js"></script>
<script type="text/javascript" src="sub/child_partial.js"></script>

<h2>Render in browser</h2>
<hr/>

<div id="container" style="padding: 30px; border: solid 1px #ccc;"></div>


<script type="text/javascript">
    window.onload = function() {
        var partial = window.render.partial;
        var child_partial = window.render.sub.child_partial;
        var container = document.getElementById('container');

        container.insertAdjacentHTML('beforeEnd', partial());

        setTimeout(function(){
            container.insertAdjacentHTML('beforeEnd', child_partial());
        }, 1500);
    };
</script>
```


Still useing the file extension rules of [doT](http://olado.github.com/doT), but you can custom the rules.

    this.__write_to_file  = o.writeToFile || true;  // only cluster.isMaster should be write to file
    this.__auto_clear     = o.autoClear;           // Automatically clears the compiled directory
    this.__layout_exp     = options.layoutExp    || /\.ly(\.def|\.dot|\.jst|\.html|\.htm)?$/,
	this.__partial_exp    = options.partialExp   || /\.def(\.dot|\.jst|\.html|\.htm)?$/,
	this.__fn_exp         = options.fnExp        || /\.dot(\.def|\.jst|\.html|\.htm)?$/,
	this.__file_exp       = options.fileExp      || /\.jst(\.dot|\.def|\.html|\.htm)?$/,


## Example for express

[With express](https://gitlab.com/Neal80/dot-layout/tree/master/examples)


## License
[MIT](https://gitlab.com/Neal80/dot-layout/blob/master/LICENSE)