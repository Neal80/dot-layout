const path = require('path');
const express = require('express');
const dot = require('../index'); // dot-layout
const app = express();

const render = dot.process({
    autoClear: false, 
    path: path.join(__dirname, 'templates'),
    destination: path.join(__dirname, 'views'),
});

// This is no longer important
app.set('views', './views');
app.set('view engine', 'dot');

app.use(express.static(path.join(__dirname, 'views')));

app.get('/', (req, res) => res.send(render.home()));
app.get('/login', (req, res) => {
    const model = {
        title: 'Welcome to login',
        helpFn: () => true,
    };

    res.send(render.sub.login(model));
});
app.get('/browser', (req, res) => res.send(render.browser()));


app.listen(3000, () => console.log('Example app listening on port 3000!'));