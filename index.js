const path = require('path');
const fs = require("fs");
const doT = module.exports = require("dot");

const layoutDef = '{{#layout';
const layoutDefine = 'layout.';  // {{#layout.file path}}
const bodyDef = '{{#body}}';


doT.__express = function(options) {
	/* not implement */
    return function(file_path, o, callback) {		
        callback(null, 'Something broke in engine.');
    };
};

doT.process = function(options) {
	//path, destination, global, rendermodule, templateSettings
	return new InstallDots(options).compileAll();
};

/*
	only load to func, not compiled to file
	if you run app useing the cluster.
	you can process in the master, load in the worker
*/
doT.load = function(options) {
	return new InstallDots({
		...options,
		writeToFile: false,
	}).compileAll();
}

function InstallDots(o) {
	this.o                = o;
	this.__write_to_file  = o.writeToFile || true;  // only cluster.isMaster should be write to file
	this.__auto_clear     = o.autoClear;  // Automatically clears the compiled directory
	this.__layout_exp     = o.layoutExp    || /\.ly(\.def|\.dot|\.jst|\.html|\.htm)?$/;
	this.__partial_exp    = o.partialExp   || /\.def(\.dot|\.jst|\.html|\.htm)?$/;
	this.__fn_exp         = o.fnExp        || /\.dot(\.def|\.jst|\.html|\.htm)?$/;
	this.__file_exp       = o.fileExp      || /\.jst(\.dot|\.def|\.html|\.htm)?$/;
	this.__path 		  = path.join(o.path || "./");
	if (this.__path[this.__path.length-1] !== path.sep) this.__path += path.sep;
	this.__destination	  = path.join(o.destination || this.__path);
	if (this.__destination[this.__destination.length-1] !== path.sep) this.__destination += path.sep;
	this.__global		  = o.global || "window.render";
	this.__rendermodule	  = o.rendermodule || {};
	this.__settings 	  = o.templateSettings ? copy(o.templateSettings, copy(doT.templateSettings)) : undefined;
	this.__includes		  = {};
	this.__includes_files = {};
	this.__layouts        = {};
	this.__templates      = {};
}

InstallDots.prototype.compileToFile = function(filePath, template, def) {
	def = def || {};
	var moduleName
		, modulePaths = filePath.replace(this.__destination, '').split(path.sep)
		, registerCode = []
		, stepPath = `${this.__global}`
        , defs = copy(this.__includes, copy(def))
		, settings = this.__settings || doT.templateSettings
		, compileoptions = copy(settings)
		, defaultcompiled = doT.template(template, settings, defs)
		, exports = []
		, compiled = ""
		, fn
		, dirname;

	// ./some/page.jst module name is some_page
	registerCode.push(`${this.__global}=${this.__global} || {};`);
	for (let i = 0; i < modulePaths.length - 1; i++) {
		const item = modulePaths[i];
		stepPath = `${stepPath}['${item}']`;
		registerCode.push(`${stepPath}=${stepPath} || {};`);
	}
	moduleName = modulePaths[modulePaths.length -1];
	moduleName = moduleName.substring(0, moduleName.lastIndexOf("."));
	stepPath = `${stepPath}['${moduleName}']`;
	registerCode.push(`${stepPath}=itself;`);
	
	for (var property in defs) {
		if (defs[property] !== def[property] && defs[property] !== this.__includes[property]) {
			fn = undefined;
			if (typeof defs[property] === 'string') {
				fn = doT.template(defs[property], settings, defs);
			} else if (typeof defs[property] === 'function') {
				fn = defs[property];
			} else if (defs[property].arg) {
				compileoptions.varname = defs[property].arg;
				fn = doT.template(defs[property].text, compileoptions, defs);
			}
			if (fn) {
				compiled += fn.toString().replace('anonymous', property);
				exports.push(property);
			}
		}
	}
	compiled += defaultcompiled.toString().replace('anonymous', moduleName);

	dirname = path.dirname(filePath);
	if (!fs.existsSync(dirname)) mkdir(dirname);

	fs.writeFileSync(filePath, "(function(){" + compiled
		+ "var itself=" + moduleName + ", _encodeHTML=(" + doT.encodeHTMLSource.toString() + "(" + (settings.doNotSkipEncoded || '') + "));"
		+ addexports(exports)
		+ "if(typeof module!=='undefined' && module.exports) module.exports=itself;else if(typeof define==='function')define(function(){return itself;});else {"
		+ registerCode.join('') +"}}());");
};

function addexports(exports) {
	var ret = '';
	for (var i=0; i< exports.length; i++) {
		ret += "itself." + exports[i]+ "=" + exports[i]+";";
	}
	return ret;
}

function copy(o, to) {
	to = to || {};
	for (var property in o) {
		to[property] = o[property];
	}
	return to;
}

function readdata(path) {
	var data = fs.readFileSync(path);
	if (data) return data.toString();
	console.error("problems with " + path);
}

// mkdir -p
function mkdir(dirname) {
	let dir = '';
	dirname.split(path.sep).forEach((d) => {
		dir = path.join(dir, d !== path.sep ? `${d}/` : d); // window/linux
		if (!fs.existsSync(dir)) fs.mkdirSync(dir);
	});
}

InstallDots.prototype.compilePath = function(data) {
	if (data) {
		return doT.template(data, this.__settings || doT.templateSettings, copy(this.__includes));
	}
};

InstallDots.prototype.resolveLayout = function(data) {
	if (!data || data === null) return data;

	let content = (typeof data === "string") ? data : data.toString();

	/*
		Regular expressions don't work for some files, not all on mac os

		let def = layoutDefReg.exec(data.toString());
		if (def === null) return data;
		let define = def[0];
		let code = define.replace(/[\{|#|\}]+?/g, '').trim();
		let index = def.index;
	*/
	
	let index = content.indexOf(layoutDef);
	if (index === -1) return data;

	let end = content.indexOf('}}') + 2;
	let define = content.substring(index, end);
	let code = define.replace(/[\{|#|\}]+?/g, '').trim();
	
	const name = path.join(code.substring(layoutDefine.length)).split(path.sep).join('_');
	const layoutContent = this.__layouts[name];

	if (layoutContent) {
		content = content.replace(define, '');

		index = layoutContent.indexOf(bodyDef);
		if (index === -1) {
			throw new Error('Undefined {{#body}} in layout.');
		}

		return layoutContent.slice(0, index) + content + layoutContent.slice(index + bodyDef.length);
	}

	return data;
}

InstallDots.prototype.resolveTemplates = function() {
	/*
		templates = {
			home_dot: {
				"name": "home_dot.dot.html",
				"path": "F:\\site\\templates\\home_dot.dot.html"
			},
			layouts\layout_login: {
				"name": "layout_login.ly.html",
				"path": "F:\\site\\templates\\layouts\\layout_login.ly.html"
			}
		};
	*/
	const self = this;
	const basePath = path.join(this.__path);
	let key, absolutePath, stats;

	const resolve = function(dir) {
		fs.readdirSync(dir).forEach((filename) => {
			absolutePath = path.join(dir, filename);
            stats = fs.statSync(absolutePath);
            
			if (stats.isDirectory()) {
				resolve(absolutePath);
			} else {
				key = absolutePath.replace(basePath, '');
				key = key.substring(0, key.indexOf('.'));

				self.__templates[key] = {
					name: filename,
					path: absolutePath,
				};
			}
		});
	};

	resolve(basePath);
};

// rm -rf path
InstallDots.prototype.clearAll = function(dir) {
	let absolutePath, stats;

	const clear = function(d) {
		if (!fs.existsSync(d)) return;

		fs.readdirSync(d).forEach((filename) => {
			absolutePath = path.join(d, filename);
            stats = fs.statSync(absolutePath);
            
			if (stats.isDirectory()) {
				clear(absolutePath);
			} else {
				fs.unlinkSync(absolutePath);
			}
		});
	};

	clear(dir);
}

InstallDots.prototype.compileAll = function() {
	if (this.__auto_clear) {
		if (doT.log) console.info("Clear all compiled templates.");
		this.clearAll(this.__destination);
	}
	
	if (doT.log) console.info("Compiling all doT templates...");

	this.resolveTemplates();

	const buildTemplates = (fn) => {
		Object.keys(this.__templates).forEach((k) => 
			fn(this.__templates[k].name, k, this.__templates[k].path));
	};

	const registerModule = (relativePath, to, data) => {
        const namePaths = relativePath.split(path.sep);
        
		let refs = to;
		namePaths.forEach((x, i) => {
			refs[x] = (i < namePaths.length - 1) ? refs[x] || {} : refs[x] = data;
			refs = refs[x];
		});
	};

	// build layouts
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__layout_exp.test(name)) {
			if (doT.log) console.info(`Loaded layout: ${relativePath}`);
			this.__layouts[relativePath.split(path.sep).join('_')] = readdata(absolutePath);
		}
	});

	/*
		build def partials
		{{#def.subFolder.block}}
	*/
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__partial_exp.test(name)) {
			if (doT.log) console.info(`Loaded def: ${relativePath}`);
			const data = this.resolveLayout(readdata(absolutePath));
			registerModule(relativePath, this.__includes, data);

			if (this.__write_to_file) {
				this.__includes_files[relativePath] = data;
			}
		}
	});

	// build jst and dot
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__fn_exp.test(name)) {
			if (doT.log) console.info(`Compiling ${relativePath} to function`);
			const data = this.resolveLayout(readdata(absolutePath));
			registerModule(relativePath, this.__rendermodule, this.compilePath(data));
        }
        
		if (this.__file_exp.test(name)) {
			if (doT.log) console.info(`Compiling ${relativePath} to file`);
			const data = this.resolveLayout(readdata(absolutePath));

			if (this.__write_to_file) {
				this.compileToFile(path.join(this.__destination, `${relativePath}.js`), data);
			}

			// for server render
			registerModule(relativePath, this.__rendermodule, this.compilePath(data));
		}
	});


	// def partials write to file
	buildTemplates((name, relativePath) => {
		if (this.__partial_exp.test(name) && this.__includes_files[relativePath]) {
			if (doT.log) console.info(`Compiling def: ${relativePath} to file`);
			
			this.compileToFile(path.join(this.__destination, `${relativePath}.js`), this.__includes_files[relativePath]);
		}
	});


	return this.__rendermodule;
};